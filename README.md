Contao-Starrating-Bundle
=================

Die Erweiterung bietet die Möglichkeit einfach per Inserttag eine Sterne-bewertungs-Element nach belieben zu platzieren. Im Backend hat man dann die Möglichkeit ein oder mehrere Bewertungs-Konfigurationen anzulegen. Darin werden dann als Kind-Elmeente die dazugehörenden Seiten mit deren Bewertungen gespeichert.

Als zweiter Paramter wird die ID oder der Alias der Konfiguration benötigt.
`{{starrating::ID | ALIAS}}`
